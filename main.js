const path = require('path')
const os = require('os')

const { app, BrowserWindow, ipcMain, shell } = require('electron')

const imagemin = require('imagemin')
const imageminMozjpeg = require('imagemin-mozjpeg')
const imageminPngquant = require('imagemin-pngquant')
const slash = require('slash')
const log = require('electron-log')


process.env.NODE_ENV = 'production'

let mainWindow

function createMainWindow () {
    mainWindow = new BrowserWindow({
        title: 'Lightify',
        width: 500,
        height: 400,
        backgroundColor : '#FFF',
        webPreferences:{
            nodeIntegration: true,
        }
    })

    mainWindow.loadURL(`file://${__dirname}/app/index.html`)
}

app.on('ready', () => {
    createMainWindow();
    mainWindow.on('closed', () => mainWindow = null)
})



ipcMain.on('image:minimize', (e, options) => {
    options.dest = path.join(os.homedir(), 'Lightify')
    resizeImage(options)
})

async function resizeImage({ imgPath, quality, dest }){
    try {
        const pngQuality = quality / 100
        const files = await imagemin([slash(imgPath)], {
            destination: dest,
            plugins: [
                imageminMozjpeg({ quality }),
                imageminPngquant({ 
                    quality: [pngQuality, pngQuality]
                 })
            ]
        })
        shell.openPath(dest)

        log.info(files)

        mainWindow.webContents.send('image:done')
    } catch (error) {
        log.error(error)
    }
}

app.on('window-all-closed', () => {
    app.quit()

  })
  
  app.on('activate', () => {
    if (BrowserWindow.getAllWindows().length === 0) {
      createMainWindow()
    }
  })

  